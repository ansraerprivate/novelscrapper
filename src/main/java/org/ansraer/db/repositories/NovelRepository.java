package org.ansraer.db.repositories;

import org.ansraer.db.models.Novel;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * @author Ansraer on 08.05.2018
 * @project NovelScrapper
 */

@Transactional
public interface NovelRepository extends CrudRepository<Novel, Long> {


    /**
     * This method will find an User instance in the database by its webnovel.com bookID.
     * Note that this method is not implemented and its working code will be
     * automagically generated from its signature by Spring Data JPA.
     */
    public Novel findByBookid(long bookid);

}
