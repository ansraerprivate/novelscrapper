package org.ansraer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

import javax.sql.DataSource;

/**
 * @author Ansraer on 08.05.2018
 * @project NovelScrapper
 */

@Configuration
public class AppConfig {

    @Value("${server.mode.standalone}")
    boolean STANDALONE;


    @Bean
    public DataSource dataSource() {
        if(STANDALONE){
             DataSource dataSource = DataSourceBuilder.create()
                    .username("spring")
                    .password("zTDBqxEufnVezsVv")
                    .url("jdbc:mysql://localhost/novelscrapper")
                    .driverClassName("com.mysql.jdbc.Driver")
                    .build();
            return dataSource;
        } else {
            JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
            DataSource dataSource = dataSourceLookup.getDataSource("jboss/datasources/defaultDS");//default ist: jdbc/apolloJNDI
            return dataSource;
        }
    }


    @Bean
    public JdbcTemplate jdbcTemplate(DataSource ds) {
        return new JdbcTemplate(ds);
    }

}
