package org.ansraer.webcrawler;

import org.ansraer.db.models.Novel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

/**
 * @author Ansraer on 08.05.2018
 * @author Jakob Lang
 * @project NovelScrapper
 */
public class NovelWebDriver {


    WebDriver driver;

    public NovelWebDriver(){

        this.driver = new FirefoxDriver();
    }

    public Novel getNovelInfo(long bookId){

        Novel novel = new Novel();
        novel.setBookid(bookId);

        driver.get("https://www.webnovel.com/book/"+bookId);

        //getting title
        WebElement titleElement = driver.findElement(By.xpath("//h2[contains(@class, 'mb15 lh1d2')]"));
        String bookName = titleElement.getText();
        //remove other parts (like author) from title
        List<WebElement> children = titleElement.findElements(By.xpath(".//*"));
        for(WebElement c : children){
            bookName.replace(c.getText(), "");
        }
        novel.setName(bookName);

        //getting total chaps
        WebElement totalChaptersElement = driver.findElement(By.xpath("//*[contains(text(), ' Chapters')]"));
        String totalChapters = totalChaptersElement.getText();
        totalChapters.replace(" Chapters","");
        novel.setTotalChapters(Integer.parseInt(totalChapters));

        //getting chaps

        int max_chaps=0;
        int max_free_chaps=0;

        //first we click the toc link:
        driver.findElement(By.cssSelector("a[href*='#contents']")).click();
        for(WebElement volume :  driver.findElements(By.className("volume-item"))){
            for(WebElement chapter : driver.findElements(By.className("w50p"))){

                int number = Integer.parseInt(chapter.findElement(By.tagName("i")).getText());//gets chapter number
                if(number>max_chaps) max_chaps=number;

                boolean free = chapter.findElement(By.tagName("svg"))==null;

                if(free && number>max_free_chaps) max_free_chaps = number;
            }
        }

        novel.setTotalChapters(max_chaps);
        novel.setFreeChapters(max_free_chaps);


        driver.close();

        return novel;
    }


}
