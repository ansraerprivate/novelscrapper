package org.ansraer.controller;

import org.ansraer.db.repositories.NovelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Ansraer on 08.05.2018
 * @author Jakob Lang
 * @project NovelScrapper
 */
@Controller
public class HomeController {

    @Autowired
    NovelRepository novelRepository;

    @RequestMapping(value = {"/home","/"}, method = RequestMethod.GET)
    public ModelAndView showHome() {
        System.out.println("Showing home");

        ModelAndView mav = new ModelAndView("home");
        //mav.addObject("novels", novelRepository.findAll());
        return mav;
    }

}
