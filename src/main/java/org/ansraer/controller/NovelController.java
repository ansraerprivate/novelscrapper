package org.ansraer.controller;

import org.ansraer.db.models.Novel;
import org.ansraer.db.repositories.NovelRepository;
import org.ansraer.webcrawler.NovelWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Ansraer on 08.05.2018
 * @author Jakob Lang
 * @project NovelScrapper
 */


@Controller
@RequestMapping("/book")
public class NovelController {

    @Autowired
    private NovelRepository novelRepository;

    @RequestMapping(value="/add/{bookId}", method=RequestMethod.GET)
    public String addBook(@PathVariable("bookId") long bookID) {
        System.out.println("adding new book");
        //create stuff here

        NovelWebDriver novelDriver = new NovelWebDriver();

        Novel book = novelDriver.getNovelInfo(bookID);

        if(book != null) novelRepository.save(book);

        return "redirect:/";
    }


    @RequestMapping(value="/{bookId}", method=RequestMethod.GET)
    public String showBook(@PathVariable("bookId") long bookID) {

        //create stuff here
        System.out.println("Showing book info");


        Novel book = novelRepository.findByBookid(bookID);


        return "redirect:/";
    }

}
