package org.ansraer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.event.EventListener;


/**
 * @author Ansraer on 21.04.2018
 * @project NovelScrapper
 */
@SpringBootApplication
public class NovelScrapper  extends SpringBootServletInitializer {

    /**This is necessary should we want to run our webapp as a war on a tomcat server.
     *
     * @param application
     * @return
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        //external server

        return application.sources(NovelScrapper.class).properties(
                "server.mode.standalone:false");
    }
    public static void main(String[] args)  throws Throwable {
        //embedded server

        new SpringApplicationBuilder(NovelScrapper.class).properties(
                "server.mode.standalone:true").run(args);
    }



    @Value("${server.mode.standalone}")
    boolean standalone;

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        System.out.println("Started...");
        System.out.println("This is standalone " + standalone);

    }
}
